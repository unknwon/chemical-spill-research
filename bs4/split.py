#!/usr/bin/python

import os
import nltk
from nltk import tokenize

nltk.data.path.append('nltk_data')

for dirname, dirnames, filenames in os.walk('clean'):
    for filename in filenames:
        if filename == ".DS_Store":
            continue
        fpath = os.path.join(dirname, filename)
        with open(fpath, 'r') as content_file:
            sentences = tokenize.sent_tokenize(content_file.read().decode('utf-8'))
            new_fpath = fpath.replace("clean/", "ss/", 1)
            print new_fpath
            if not os.path.exists(os.path.dirname(new_fpath)):
                os.makedirs(os.path.dirname(new_fpath))
            target = open(new_fpath, 'w')
            target.write('\n'.join(sentences).encode('utf-8'))
            target.close()
