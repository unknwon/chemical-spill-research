#!/usr/bin/python

import os
import re
from bs4 import BeautifulSoup
import pdf


def visible(element):
    if element.parent.name in ['style', 'script', '[document]', 'head', 'title', 'footer']:
        return False
    elif re.match('<!--.*-->', element.encode('utf-8')):
        return False
    return True


def get_text(fpath):
    with open(fpath, 'r') as content_file:
        soup = BeautifulSoup(content_file.read(), "html.parser")
        texts = soup.findAll('p')
        texts = filter(visible, texts)
        texts = [(''.join(x.findAll(text=True))).encode('utf-8').strip() for x in texts]
        s = '\n'.join(texts)
        # print s
        new_fpath = fpath.replace("data/", "clean/", 1) + '.txt'
        print new_fpath
        if not os.path.exists(os.path.dirname(new_fpath)):
            os.makedirs(os.path.dirname(new_fpath))
        target = open(new_fpath, 'w')
        target.write(s)
        target.close()


def walk():
    for dirname, dirnames, filenames in os.walk('data'):
        # print path to all filenames.
        for filename in filenames:
            if filename.endswith(".pdf"):
                new_fpath = os.path.join("clean", filename) + '.txt'
                print(new_fpath)
                if not os.path.exists(os.path.dirname(new_fpath)):
                    os.makedirs(os.path.dirname(new_fpath))
                target = open(new_fpath, 'w')
                try:
                    target.write(pdf.convert_pdf_to_txt(os.path.join(dirname, filename)))
                except:
                    pass
                target.close()

            else:
                get_text(os.path.join(dirname, filename))


if __name__ == '__main__':
    wd = os.getcwd()
    print 'Working directory: ' + wd
    walk()
