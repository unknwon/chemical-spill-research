
13 January 2014 Last updated at 18:01 GMT
West Virginia authorities have begun lifting a ban on drinking tap water enacted after a chemical spill tainted much of the state's water supply.
Governor Earl Ray Tomblin told reporters some of the 300,000 people affected could begin using the water.
"The numbers we have today look good and we are finally at a point where the 'do not use order' has been lifted," he said.
On Thursday, a chemical used to process coal leaked from a plant into the Elk River.