Advertisement
Advertisement
By THE ASSOCIATED PRESSOCT.
7, 2015, 2:49 P.M. E.D.T.
Advertisement
CHARLESTON, W.Va. —  More than $2 million will be distributed to residents and businesses affected by a 2014 chemical spill in West Virginia under a liquidation plan approved by a bankruptcy judge.
Freedom Industries' plan also will provide $1.4 million to the West Virginia Department of Environmental Protection and environmental firms for continued cleanup work.
Freedom's parent, Chemstream Holdings, is adding $1.1 million for the cleanup under an agreement with the agency.
A $350,000 initial cash payment will be distributed to unsecured claims, plus future payments from potential recovery sources.
"The plan is the result of extensive arm's-length discussions, debate and/or negotiations among the debtor and key stakeholders and is overwhelmingly supported by the creditors and other parties-in-interest in the case," U.S. Bankruptcy Judge Ronald Pearson wrote in a Tuesday order approving the plan.
Pearson said liquidating the company under Chapter 7 would have reduced the amount of proceeds available for distribution to claim holders because of increased administrative fees and other costs.
Pearson also approved the appointment of Robert Johns to administer the plan, along with a spill claim oversight committee.
Johns and the committee will determine how funds will be used or distributed.
The plan's funding sources include about $2.7 million in sale escrow fund proceeds, $300,000 from a settlement with former Freedom President Gary Southern and $3.1 million from a settlement with Freedom's insurer, AIG.
Coal-cleaning chemicals spilled into the Elk River from a Freedom Industries' tank in Charleston on Jan. 9, 2014.
The spill contaminated tap water for more than 300,000 water customers for days.
Businesses that couldn't operate without water, including restaurants, and individuals filed claims in bankruptcy court seeking compensation.
Southern and five other former Freedom executives pleaded guilty earlier this year to federal pollution violations.
Go to Home Page »