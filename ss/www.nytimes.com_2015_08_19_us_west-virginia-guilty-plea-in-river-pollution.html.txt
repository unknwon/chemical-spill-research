Advertisement
Advertisement
By THE ASSOCIATED PRESSAUG.
18, 2015
Advertisement
One of the last executives charged in a chemical spill that left 300,000 people without clean tap water for days pleaded guilty to federal pollution violations Tuesday.
Dennis Farrell, a former Freedom Industries owner, pleaded guilty in federal court in Charleston, joining the bankrupt company itself and four other former Freedom officials who had already pleaded guilty.
The deal calls for a sentence of 30 days to two years in prison, as well as a maximum $200,000 fine.
The former company president, Gary Southern, the final and highest-profile executive targeted for the spill, is expected to plead guilty Wednesday.
In January 2014, a run-down Freedom tank in Charleston leaked coal-cleaning chemicals into the water supply for nine counties.
A version of this brief appears in print on August 19, 2015, on page A14 of the New York edition with the headline: West Virginia: Guilty Plea in River Pollution.
Order Reprints| Today's Paper|Subscribe
Go to Home Page »