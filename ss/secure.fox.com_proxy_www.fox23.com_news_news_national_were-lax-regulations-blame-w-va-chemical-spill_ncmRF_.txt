View mobile site
View mobile site
Follow us on
Tuesday, Oct. 13, 2015 | 7:15 p.m.
Hi,  (not you?)
| Member Center | Sign Out
Sign In | Register
Search
            cmg.query(function(){
                var search_fields = document.querySelectorAll("[name=q]");

                for (var i=0; i < search_fields.length; i++) {
                    search_fields[i].setAttribute("value", cmg.utility.loc_query('q'));
                }
            });
Breaking News▸
Posted: 11:02 a.m. Monday, Jan. 13, 2014
By ELIZABETH HAGEDORN
Video transcript provided by Newsy.com
Days after a chemical leak first tainted West Virginia's tap water, thousands of residents are still waiting to hear when it will be safe to use again.
But another question has emerged in the aftermath.
How was a storage facility containing industrial chemicals allowed to store them so close to the state’s major water supply?
(Via BBC)
Freedom Industries is the company responsible for the leak.
It uses a chemical known as crude MCHM to clean coal after it’s mined from the ground.
And it seems authorities were aware the company was storing up to 1 million pounds of the chemical  — an estimated 7,500 gallons of which leaked into the Elk River last week, shutting down schools and businesses across the state.
(Via CBS)
That’s according to the Charleston Gazette, which reports Freedom Industries informed the state about a year ago it was storing large quantities of the potentially dangerous chemical.
A chemical safety expert questioned why that was overlooked, telling the Gazette: “Obviously, the whole idea of the chemical inventory reports is to properly inform local emergency officials about the sorts of materials they might have to deal with  … It’s just head-in-the-sand to be ignoring this type of threat.” ​
The Wall Street Journal reports the reason the company largely avoided state and federal oversight is because the coal-cleaning chemical was used mainly for storage, not manufacturing or processing.
It's been more than 20 years, according to The New York Times, since environmental inspectors checked up on the company.
Possibly most troubling — not much is known about the effects of industrial chemical.
And as West Virginia Gov.
Earl Ray Tomblin told reporters, Freedom Industries hasn't provided much guidance.
"It's one that we've had to do a lot of research on ...
I think that perhaps they could have been a little more forthcoming.
" (Via CNN)
Federal authorities have opened an investigation into the spill, and will look at, among other things, why it took the company responsible for the leak hours to report it.
- See more at newsy.com.
Trending on Facebook
More Popular and trending stories
© 2015 Cox Media Group.
By using this website,
    you accept the terms of our Visitor Agreement and Privacy Policy, and understand your options regarding Ad Choices.This station is part of Cox Media Group TelevisionLearn about careers at Cox Media Group
View mobile site
View mobile site
Already have an account?
Sign In
Already have an account?
Sign In
We have sent you a confirmation email.
Please check your email and click on the link to activate your account.
We look forward to seeing you frequently.
Visit us and sign in to update your profile, receive the latest news and keep up to date with mobile alerts.
Don't worry, it happens.
We'll send you a link to create a new password.
We have sent you an email with a link to change your password.
We've sent an email with instructions to create a new password.
Your existing password has not been changed.
To sign in you must verify your email address.
Fill out the form below and we'll send you an email to verify.
Check your email for a link to verify your email address.