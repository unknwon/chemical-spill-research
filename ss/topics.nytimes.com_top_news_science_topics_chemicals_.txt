Glass maker Guardian Industries reaches settlement with Justice Department and Environmental Protection Agency over charges it violated Clean Air Act by emitting illegal amounts of chemical pollutants at plants in seven states.
MORE
Chemical factory explosion in China's Shandong Province kills one and injures at least nine; blast increases anxiety about China’s expanding chemical industry and lax regulatory enforcement.
MORE
Chinese authorities do not explain thousands of dead fish on banks of Haihe River, less than four miles from site in port city of Tianjin where series of deadly explosions destroyed warehouse filled with toxic chemicals, killing more than 100 people; photos of masses of dead fish are posted on Chinese social media, increasing public fears over chemical contamination, despite government assurances of environmental safety in area.
MORE
Former Freedom Industries executive Dennis Farrell pleads guilty to federal pollution violations; is one of last charged in connection with West Virginia chemical spill responsible for leaving 300,000 people without clean tap water for days.
MORE
Congress is set to vote on bill that would update Toxic Substances Control Act, which falls far short of what is needed to prevent accidents like Elk River chemical spill in West Virginia.
MORE
Nicholas Kristof Op-Ed column warns that class of chemicals known as PFASs have become ubiquitous in consumer products despite their known cancer risk; argues they are 'poster child' for lax chemical regulation in the United States, where vast majority of 80,000 chemicals have never been tested for health effects; laments that chemical industry has spent some $190 million on lobbying in last three years.
MORE
Editorial applauds Congressional effort to reform Toxic Substances Control Act but criticizes measures under consideration for failing to speedily evaluate most worrisome chemicals; contends Senate bill is stronger than House bill in pushing Environmental Protection Agency to take action on dangerous chemicals, but says neither bill is sufficient.
MORE
Series of studies published in journal Environmental Health Perspectives raises urgent concerns about new generation of poly- and perfluoroalkyl substances, chemicals that are used in thousands of common consumer products; previous generations of PFAs were shown to increase risk of cancer; scientists, including Linda S Birnbaum of the Health and Human Services Dept, say new crop has been insufficiently studied and may carry similar risks.
MORE
Editorial examines question of what to do about two bills introduced in Senate that differ significantly in their approaches to reforming Toxic Substances Control Act, which has failed to ensure safety of chemicals used in consumer products; suggests improvements to bill proposed by Sens Tom Udall and David Vitter, which already has bipartisan support.
MORE
New Mexico Sen Tom Udall is helping to negotiate legislation that would change how government evaluates safety of chemicals; critics of negotiations say Udall has been too open to pressure from chemical industry and his draft is written in way that protects profits more than public health.
MORE
Campaign by safety and consumer advocacy groups to reduce use of preservative methylisothiazolinone, or MI, in variety of personal and household items illustrates challenges product manufacturers face as they respond to complaints about potentially harmful chemicals; MI has caused serious allergic skin reactions in some consumers; makers question whether specific chemicals are in fact harmful, and assert that limiting use of some chemicals will restrict choices for consumers.
MORE
Federal grand jury indicts four owners and operators of Freedom Industries, which contaminated Elk River in West Virginia with toxic chemical spill in January; spill caused extended cutoff of drinking water to almost 300,000 residents in and near Charleston.
MORE
Op-Ed article by policy experts Rolf U Halden and Robert S Lawrence calls for reform to the United States regulatory system and manufacturing approach for chemicals; observes that on average it takes 14 years for government to act on known safety issues, problem that is demonstrated by triclosan and triclocarban, antibacterial contaminants that have been used for some 40 years; makes several recommendations toward establishing sustainable chemical regulation.
MORE
New chemicals are turning up everywhere in the environment, and the health risks are mostly unknown.
MORE
Op-Ed article by biologist Mark Winston examines global collapse of honeybee colonies; holds it was brought about in part by the complex interaction of man-made chemicals like pesticides in the environment; contends tumultuous demise of honeybees should alert human beings to their own vulnerability; calls on regulatory authorities to require studies on how exposure to low dosages of combined chemicals may affect human health before approving compounds.
MORE
Toxic chemical spill in West Virginia has spiraled into a crisis of confidence in state and federal authorities, as residents complain of confusing messages and a lack of trust in experts; spill continues to arouse fear and outrage and threatens a political crisis in a state where lawmakers have long supported the coal and chemical industries.
MORE
Parabens are used in a wide range of personal care items – from cosmetics to toothpaste, as well as some foods and drugs — but questions remain about their safety.
MORE
Tests on the water supply in Charleston, W Va, one week after a chemical spill tainted city’s water system and left 300,000 people without safe water, turns up traces of formaldehyde.
MORE
Op-Ed article by Rafael Moure-Eraso, chairman of the United States Chemical Safety Board, contends that the chemical spill in West Virginia is the latest in a series of disasters and near-misses across the country; warns that urgent steps are required to significantly improve the safety of the nation's chemical industry; contends the Environmental Protection Agency should act now to strengthen safety rules.
MORE
Editorial contends chemical spill in West Virginia has exposed serious defects in state and federal environmental protections that allow many facilities and chemicals to escape scrutiny; maintains that passing of the crisis, as well as fact that residents can resume drinking tap water, should not dissuade the state or the federal government from strengthening and enforcing statutes.
MORE
Food and Drug Administration says it is requiring soap manufacturers to demonstrate that antibacterial chemicals used in everyday items are safe or to take them out of products altogether; agency says it is taking step after some data suggested problems like bacterial resistance and hormonal effects; proposal is applauded by public health experts, who have urged agency to regulate antimicrobial chemicals.
MORE
Nicholas D Kristof Op-Ed column praises HBO documentary Toxic Hot Seat for exposing how the inclusion of flame retardant chemicals in a majority of the furniture sold in the United States is a dizzying corporate scandal; notes that studies have shown that flame retardants have been linked to cancer, birth defects, diminished IQ's and other problems, and have not been proven effective in delaying fires.
MORE
Pres Obama orders federal agencies to review safety rules at chemical facilities in response to April 2013 explosion at a Texas fertilizer plant that killed 15 people.
MORE
Editorial praises Pres Obama for directing federal agencies overseeing chemical plants to update regulations, coordinate policies and share information with one another and state and local governments; contends question that remains is how ambitious regulators like the Environmental Protection Agency are willing to be in enacting and enforcing new rules in the face of industry opposition.
MORE
One person is killed and five injured in chemical plant explosion in Donaldsonville, La, one day after another fatal explosion at chemical plant in neighboring town of Geismar; two explosions do not appear related.
MORE
Editorial supports bipartisan Senate bill that would revise 1976 Toxic Substances Control Act  because it will be significant advance over current law.
MORE
Introduction of bipartisan Senate measure that could lead to first major update to toxic chemical law has surprised both senators and groups that have been pushing for new chemical safety law.
MORE
Editorial contends it is long past time for Congress to reform 1976 Toxic Substances Control Act so that it provides genuine protection against harmful chemicals in products like shampoos and detergents; calls on Senate to pass Safe Chemicals Act of 2013, which is opposed by both chemical industry and many Republicans.
MORE
News analysis; overwhelming majority of chemicals in consumer products today have never been independently tested for safety, largely because of the Toxic Substances Control Act, which is outdated; although there has been movement in Congress to update bill, American consumers and companies looking for safer products are largely on their own for the moment.
MORE
The Week column; Swedish study shows that exposure to anti-anxiety drug Oxazepam changes behavior of wild European perch, raising question of how ecological balance is affected when human-produced chemicals like pharmaceuticals are washed into bodies of water; other significant developments in health and science news highlighted.
MORE
Nicholas D Kristof Op-Ed column draws attention to studies being done on obesity that suggest that one factor may be endocrine-disrupting chemicals, which are largely unregulated; calls for more research on endocrine disruptors and for Congress to pass Safe Chemicals Act, which would require more stringent safety testing of potentially toxic chemicals.
MORE
Officials in coastal city of Ningbo, China, promise to halt the expansion of a petrochemical plant after thousands of demonstrators clash with police during three days of protests; unrest spotlights the public's mounting discontent with industrial pollution, and its increasing willingness to openly challenge the government in the streets.
MORE
Op-Ed article by former New Jersey Gov Christine Todd Whitman calls for legislation giving the Environmental Protection Agency the authority needed to reduce the vulnerability of chemical facilities to act of terrorism; holds providing the EPA with the ability to require that chemical plants evaluate use of safer chemicals and processes is both good policy and good politics in a world continuing to deal with threat of terrorism.
MORE
Nicholas D Kristof Op-Ed column warns that there is growing evidence that chemicals like bisphenol-A, found in everyday items, could be hazardous to everyone's health; observes that chemical companies have blocked any serious regulation of such substances.
MORE
Editorial urges the Environmental Protection Agency, under the Clean Air act, to impose new safety rules on the country's chemical plants; warns plants remain dangerously unprepared for large-scale accidents and terrorist attacks; maintains unilateral action by an agency is necessary due to Congressional gridlock.
MORE
There are no additional abstracts to display.
The effort, led by Gov.
Pete Ricketts, is a blow to legislators and others who led a successful effort four months ago to end the death penalty.
As Asian-American fraternities have spread in recent decades, some have replicated not only the camaraderie of other Greek organizations, but also their excesses.
The move came one day after Athletic Director Pat Haden announced that Sarkisian would take a leave of absence after he failed to show up for a practice.
Despite a Supreme Court ruling allowing a controversial drug to be used for lethal injections, states are not finding it easier to execute inmates.
The Yankees supported pitcher C. C. Sabathia after he announced on the eve of their wild-card appearance against the Houston Astros that he would enter an alcohol rehabilitation center.
The pitcher acknowledged that his decision would hurt his team, but said, “I owe it to myself and to my family to get myself right.”
A doctor recommends a concerted effort by students and administrators to stop sexual assault and binge drinking on campus.
The agreement with Guardian, which will spend $70 million to control emissions from plants in seven states, is the latest effort to crack down on industrial polluters.
China announced that it would cut 300,000 troops, its largest reduction in nearly two decades.
The mystery of scents inspires a personal research trial.
Subscribe to an RSS feed on this topic.
What is RSS?
Receive My Alerts e-mails on topics covered on this page.