(RNN) - No matter how your 2014 went, there's one thing that's true – it was a roller coaster of news.
2014 was a busy year full of crises, international political wrangling, missing airplanes, a deadly virus, scientific triumph and civil rights protests.
It was also a year of terrorism violence, massive vehicle recalls, NFL domestic violence policy backlash, Bill Cosby rape allegations and an invasive Sony Hack that saw Americans watching comedy The Interview as the patriotic thing to do.
And don't forget two big sporting events – the 2014 winter games in Sochi, Russia, and the FIFA World Cup.
By the end of the year the world map had changed, four large passenger planes crashed or disappeared, the CIA released a more than 6,000-page document commonly dubbed the "Torture Report" and space probe Philae landed on a comet millions of miles away.
Check out our top news of 2014 timeline to see just what a world-changing newsmageddeon this past year was.
Copyright 2014 Raycom News Network.
All rights reserved.