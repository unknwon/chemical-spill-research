Prepare My Family for an Emergency
Make My Pet Part of My Emergency PlanPrepare for a Natural Hazard
Be informed.
Make a Plan.
Build a Kit.
Get Involved.Click here for more information.
Do you or does someone you know have a disability?Learn how to plan for emergencies.
If you’re a health care professional in West Virginia & willing to assist during a public health emergency, then we need you...click here to read more
Hey kids...Learn about how to prepare for emergencies.Click here for children’s activities!
For more information, contact Yolanda.K.Sowards@wv.gov.
Privacy, Security and Accessibility | WV.gov | USA.gov | © 2015 State of West Virginia