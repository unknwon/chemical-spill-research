Advertisement
Advertisement
By MICHAEL WINESJAN.
17, 2014
Advertisement
Freedom Industries, the West Virginia company whose chemical spill last week tainted the drinking water of more than 300,000 residents in and around Charleston, filed for Chapter 11 bankruptcy on Friday.
In documents filed in federal bankruptcy court in Charleston, a lawyer for the company stated that the spill apparently occurred after a broken water line caused the ground to freeze beneath an aging chemical storage tank, pushing an unidentified object into the bottom of the tank.
The resulting puncture allowed 7,500 gallons of 4-methylcyclohexane methanol, a chemical that washes impurities from coal, to escape the tank on Jan. 9 and leach into the Elk River, the source of drinking water for Charleston and surrounding communities.
The area’s main water intake is about one and a half miles downstream from the cluster of 13 tanks where the leak occurred.
Residents were warned not to use the water for the next five days, and 14 were hospitalized for exposure to the chemical.
Although the authorities now say that the area’s water is safe, public and even official mistrust continues to run high.
The Charleston Gazette reported Friday that the state’s Education Department had recommended that nine school districts in the region continue to use bottled water at least through next week.
A Facebook page organized by area residents maintains a list of restaurants serving bottled water, and in a random sample of stores on Friday, bottled-water sales remained brisk.
The state attorney general’s office said it was investigating reports of price-gouging related to water sales.
One Charleston resident said in an interview on Thursday that her family was ignoring assurances that the water was safe, saying it remained foul-smelling.
She spoke from a state park away from the area, where her family had gone to spend the night.
News reports stated that Freedom Industries was facing more than 25 lawsuits stemming from the leak.
At least one newspaper carried an advertisement on Friday soliciting plaintiffs for a proposed class-action suit related to the spill.
Under Chapter 11 procedures, the company would restructure its debts and reorganize to continue operating.
A Pittsburgh law firm representing it declined on Friday to comment on the filing.
Jack Begg contributed research.
A version of this article appears in print on January 18, 2014, on page A14 of the New York edition with the headline: Company in Chemical Spill Files Bankruptcy Papers.
Order Reprints| Today's Paper|Subscribe
Go to Home Page »