## Prerequisites

- Go 1.4+
- Python 2.7+

## Bootstrap

Install `gb`:

```
$ go get github.com/constabulary/gb/...
```

Go to subdirectory `go` and execute:

```
$ gb build
```

This command should build `crawler` binary.

### Install Python modules

- [PDFMiner](https://github.com/euske/pdfminer/)
- [NLTK](http://www.nltk.org/) to the root directory.
- And others:

```
$ pip install beautifulsoup4
```

## Edit websites.txt

this file contains a list of website for searching articles, one per line:

```
nytimes.com
bbc.com
cnn.com
dhhr.wv.gov
fox.com
```

## Fetch articles

In the root directory and execute:

```
$ go/bin/crawler -keyword="west virginia chemical spill"
```

The default keyword is already **"west virginia chemical spill"**.

Then crawler should start fetching articles from the internet. Note that Google has anti-cralwer so do not execute this command too often.

## Extract text from articles

```
$ python bs4/clean.py
```

## Split text into sentences

```
$ python bs4/split.py
```

## Combine sentences with timestamp

This command also discards sentences that have less than X characters.

```
$ go/bin/combine -min=40
```

The default minimum characters is already **40**.