package main

import (
	"fmt"
	"os"
	// "math/rand"
	"flag"
	"io/ioutil"
	"strings"

	"github.com/Unknwon/com"
	"github.com/Unknwon/log"
	"github.com/parnurzeal/gorequest"
)

var userAgents = []string{
	"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20130406 Firefox/23.0",
	"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533 (KHTML, like Gecko) Element Browser 5.0",
	"IBM WebExplorer /v0.94', 'Galaxy/1.0 [en] (Mac OS X 10.5.6; U; en)",
	"Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)",
	"Opera/9.80 (Windows NT 6.0) Presto/2.12.388 Version/12.14",
	"Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25",
	"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1468.0 Safari/537.36",
	"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0; TheWorld)",
}

func cleanURL(url string) string {
	i := strings.Index(url, `/url?q=`)
	if i == -1 {
		return url
	}

	url = url[i+len(`/url?q=`):]

	i = strings.Index(url, `&amp;sa=`)
	url = url[:i]
	return strings.TrimSpace(url)
}

func main() {
	keyword := flag.String("keyword", "west virginia chemical spill", "search keyword")
	flag.Parse()
	*keyword = strings.Replace(*keyword, " ", "+", -1)
	log.Info("Search keyword: %s", *keyword)

	// Get website list.
	if !com.IsFile("websites.txt") {
		log.Fatal("websites.txt does not exist in working directory.")
	}
	data, err := ioutil.ReadFile("websites.txt")
	if err != nil {
		log.Fatal("Fail to read websites.txt: %v", err)
	}
	websites := strings.Split(string(data), "\n")
	maxPage := 3

	allItems := make([]string, 0, len(websites)*maxPage*10)
	for _, website := range websites {
		if len(website) == 0 {
			continue
		}
		log.Info("Fetching %s...", website)

		for page := 1; page <= maxPage; page++ {
			req := gorequest.New()
			// .Set("User-Agent", userAgents[rand.Intn(len(userAgents))])
			url := fmt.Sprintf("https://www.google.com/search?hl=en&q=site:%s+%s&start=%d", website, *keyword, page*10)
			fmt.Println(url)
			_, body, errs := req.Get(url).End()
			if len(errs) > 0 {
				fmt.Println(errs)
				continue
			}

			start := strings.Index(body, `<li class="g">`)
			if start == -1 {
				continue
			}
			body = body[start:]

			items := strings.Split(body, `<li class="g">`)
			end := strings.Index(items[len(items)-1], `</li></ol>`)
			items[len(items)-1] = items[len(items)-1][:end]
			allItems = append(allItems, items...)
		}
	}

	count := 0
	// allItems = append(allItems, "http://www.nytimes.com/2014/01/12/us/the-wait-continues-for-safe-tap-water-in-west-virginia.html")
	for _, item := range allItems {
		if len(item) == 0 {
			continue
		}
		item = cleanURL(item)

		count++

		fmt.Println(item)

		req := gorequest.New()
		_, body, errs := req.Get(item).EndBytes()
		if len(errs) > 0 {
			fmt.Println(errs)
			continue
		}

		os.MkdirAll("data", os.ModePerm)
		fname := strings.TrimPrefix(item, "http://")
		fname = strings.TrimPrefix(fname, "https://")
		fname = strings.Replace(fname, "/", "_", -1)
		if err := ioutil.WriteFile("data/"+fname, body, 0666); err != nil {
			fmt.Println(err)
			continue
		}
	}

	log.Info("Total links: %d/%d", count, len(allItems))
}
