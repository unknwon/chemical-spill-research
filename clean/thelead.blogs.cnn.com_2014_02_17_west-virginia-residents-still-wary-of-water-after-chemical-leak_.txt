Anchored by  Jake Tapper, The Lead airs at 4 p.m. ET on CNN.
We've moved! Come join us at our new show page.
(CNN) – It was a half-day for the students at Grandview Elementary in North Charleston, West Virginia, Monday, and it wasn't in observance of George Washington's birthday.
Teachers at the school reported smelling a licorice-like chemical odor this morning, a lingering problem since last month's chemical spill in the nearby Elk River. The spill left 300,000 people without access to clean water last month.

The smell was enough to send students home for the day, after complaints of headaches.
Government authorities have been arguing the water is safe for drinking, despite real fear in the community.
The national guard is now re-testing the water at the school.
Charleston West Virginia Mayor Danny Jones says he is drinking out of the tap at his house. But Senator Jay Rockefeller, D-West Virginia, said: "I wouldn't drink that water if you paid me."
Mayor Jones joins "The Lead" to discuss.
Comments are closed.
The Lead with Jake Tapper draws not only on Tapper’s deep knowledge of politics and national issues, but also seeks to examine and advance stories across a wide range of topics that demonstrate his own curiosities and interests. Compelling headlines come from around the country and the globe, from politics to money, sports to popular culture, based on news drivers of the day.
The Lead with Jake Tapper airs weekdays at 4 p.m. ET.
Get every new post delivered to your Inbox.

Join 140 other followers

