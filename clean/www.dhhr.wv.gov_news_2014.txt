12/11/2014
Results from the Bureau for Behavioral Health and Health Facilities (BBHHF) annual statewide survey indicate that 87.8 percent of retailers inspected did not sell tobacco products to youth under the age of 18 during 2014. ...more
12/05/2014
The West Virginia Department of Health and Human Resources Division of Immunization Services is encouraging individuals to get their flu shot as part of National Influenza Vaccination Week, December 7-13.  ...more
12/04/2014
West Virginia Department of Health and Human Resources Cabinet Secretary Karen L. Bowling today announced the appointment of Kimberly A. Walsh as Acting Chief Executive Officer of William R. Sharpe, Jr. Hospital, effective December 4, 2014....more
12/04/2014
West Virginia Department of Health and Human Resources Cabinet Secretary Karen L. Bowling today announced the appointment of Dr. Rahul Gupta as Commissioner of DHHR’s Bureau for Public Health and State Health Officer, effective January 1, 2015. ...more
12/02/2014
Change the Future West Virginia will recognize community leaders who have supported the program’s goals and objectives at 6:00 p.m., on Wednesday, December 3, at Stonewall Resort and Conference Center in Roanoke, West Virginia. ...more
10/21/2014
The West Virginia Department of Health and Human Resources Division of Immunization Services is urging pregnant women in West Virginia to get a flu shot to protect themselves and their unborn babies against the flu. ...more
10/10/2014
More teens and adults benefiting from several potentially life-saving vaccines...more
10/06/2014
DHHR program offers free or low-cost mammograms, clinical breast exams and Pap tests...more
10/03/2014
DHHR Cabinet Secretary Karen L. Bowling today announced the Bureau for Behavioral Health and Health Facilities has been awarded a $3.6 million Garrett Lee Smith State and Tribal Suicide Prevention grant...more
10/01/2014
Officials with the Bureau for Public Health today filed an order requiring health care providers and health care facilities to report to the State DHHR all cases of acute neurologic illness with focal limb weakness...more
09/22/2014
Health officials with the West Virginia Department of Health and Human Resources today said the Centers for Disease Control and Prevention confirmed four (4) cases of Enterovirus EV-D68 in West Virginia...more
09/19/2014
DHHR Cabinet Secretary Karen L. Bowling today announced the Bureau for Public Health’s progress in the implementation of public health requirements of Senate Bill 373...more
09/17/2014
DHHR Cabinet Secretary Karen L. Bowling is pleased to announce the West Virginia Department of Health and Human Resources Purchasing Office is the first West Virginia state agency to reach 100% certification......more
09/12/2014
Officials with the West Virginia Department of Health and Human Resources (DHHR) Bureau for Public Health wrapped up their 3-day meeting with the Centers for Disease Control and Prevention (CDC). ...more
09/11/2014
Continuing discussions about monitoring for possible health effects...more
08/28/2014
State Health Officer encourages residents to build a personal preparedness kit...more
08/19/2014
The West Virginia Department of Health and Human Resources received the Epi-Aid Trip Report from the Centers for Disease Control and Prevention (CDC)....more
08/14/2014
The West Virginia Department of Health and Human Resources Office of Nutrition Services today announced a new app to make shopping easier. ...more
08/04/2014
WVDHHR joins partners nationwide in recognizing August as National Immunization Awareness Month....more
07/21/2014
DHHR pursuing legal action to obtain patient listing...more
07/11/2014
Bureau for Medical Services launched the Health Homes initiative for behavioral health on July 1, 2014 and is currently accepting eligible Medicaid members...more
07/11/2014
Shots Required for Kindergarten, 7th and 12th Grade Students in West Virginia...more
07/08/2014
Findings Consistent with Other Data...more
07/07/2014
...more
07/02/2014
DHHR says clinical services will continue...more
07/01/2014
Suspected case now confirmed by CDC - The West Virginia Department of Health and Human Resources Bureau for Public Health has confirmed the state’s first case of chikungunya (pronounced chik-en-gun-ye)...more
06/27/2014
Raw data supports earlier findings by health care partners...more
06/26/2014
CDC indicates 13 states now have had a total of 80 confirmed cases...more
06/25/2014
Karen L. Bowling, secretary of the West Virginia Department of Health and Human Resources, and Rahul Gupta, executive director and health officer for the Kanawha-Charleston Health Department...more
06/24/2014
The West Virginia Department of Health and Human Resources Division of Family Assistance will begin accepting applications July 1, 2014 for school clothing vouchers ...more
06/23/2014
Preventive health screenings are one way to keep you and your family healthy. ...more
06/11/2014
West Virginia Department of Health and Human Resources Commissioner for the Bureau for Children and Families Nancy Exline announced the establishment of a Centralized Intake Unit (CIU) for reports of abuse and neglect....more
06/11/2014
Effective July 1, 2014, clinics that primarily treat patients for chronic pain must be licensed by the West Virginia Department of Health and Human Resources Office of Health Facility Licensure and Certification (OHFLAC). ...more
05/22/2014
West Virginia Department of Health and Human Resources Deputy Secretary for Public Insurance and Strategic Planning Jeremiah Samples today announced ...more
05/20/2014
WVDHHR Cabinet Secretary Karen L. Bowling today announced DHHR and its Bureau for Public Health, in partnership with the Kanawha-Charleston Health Department, will continue to seek funding ...more
05/08/2014
The West Virginia Supplemental Nutrition Program for Women, Infants and Children (WIC) is one of the State’s most important public health programs improving healthy pregnancies and birth outcomes...more
04/29/2014
West Virginia Department of Health and Human Resources Cabinet Secretary Karen L. Bowling today announced the issuance of a 6-month provisional license for the Potomac Center Main Campus...more
04/28/2014
In recognition of National Infant Immunization Week April 26 to May 3, the West Virginia Department of Health and Human Resources Division of Immunization Services highlighted the importance...more
04/22/2014
WVDHHR Cabinet Secretary Karen L. Bowling today appointed three deputy secretaries effective May 1, 2014...more
04/17/2014
WVDHHR Office of Nutrition Services today announced new U.S. Department of Agriculture (USDA) income eligibility guidelines for the Special Supplemental Nutrition Program for Women, Infants...more
04/14/2014
WVDHHR is pleased to announce that Dr. Letitia Tierney, State Health Officer and Commissioner for the Bureau for Public Health, was presented with the West Virginia Poison Center (WVPC) Distinguished Service Award ...more
04/14/2014
West Virginia DHHR today announced an updated West Virginia Sport Fish Consumption Advisory for 2014...more
04/03/2014
West Virginia Department of Health Human Resources, in conjunction with the Centers for Disease Control and Prevention (CDC), will conduct a Community Assessment for Public Health Emergency Response (CASPER)...more
04/01/2014
The Department of Health and Human Resources has historically partnered and relied on the medical expertise of the Centers for Disease Control and Prevention (CDC)," DHHR Cabinet Secretary Karen L. Bowling said...more
03/21/2014
Just over two months have passed since the West Virginia Department of Health and Human Resources Bureau for Public Health began its response to the January 9, 2014 Elk River chemical spill ...more
03/18/2014
The West Virginia DHHR, Bureau for Public Health has named Taya R. Williams as coordinator of the Office of Minority 
 
Health. ...more
03/12/2014
West Virginia Department of Health and Human Resources Cabinet Secretary Karen L. Bowling today announced the Bureau for Children and Families 
 
will begin accepting Emergency Low Income Energy Assistance Program (LIEAP)...more
03/02/2014
The West Virginia Bureau for Public Health is alerting residents to be aware of the approaching winter storm that is expected to impact many counties across the state...more
02/14/2014
The West Virginia Department of Health and Human Resources Bureau for Public Health urges citizens to install carbon monoxide detectors in their homes, especially if they are ...more
02/11/2014
Individuals owing child support may 
be eligible for a modification in the amount owed. ...more
Privacy, Security and Accessibility | WV.gov | USA.gov | © 2015 State of West Virginia